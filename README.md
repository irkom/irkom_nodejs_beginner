# IRKOM Node js for beginner #


# What is the plan? #


[x] What is the plan?

[x] What is node.js and how does it work?
    - Javascript runtime built on Chrome's V8 Javascript engine
    - Javascript running on the server
    - Used to build powerful, fast & scalable web applications
    - Uses an event-driven, non-blocking I/O model

[x] Non-blocking I/O
    - Works on a single thread using non-blocking I/O calss
    - Supports tens of thousands concurrent connections
    - Optimizes throughput and scalability in web applicatoins with many I/O operations
    - this makes Node.js apps extremely fast and efficient.

[x] Event loop
    - Single-threaded
    - supports concurrency via events and callbacks
    - EventEmitter class is used to bind events and event listeners.

[x] What we can build with Node.js?
    - REST APIs and Backend Applications
    - Real-Time Services (chat, Games, etc)
    - Blogs, CMS, Social Applications
    - Utilities And Tools
    - Anything that is not CPU-intensive

[x] Installing node.js
    - Node.js Package Manager
    - Used to install node programs/modules
    - Easy to specify and link dependencies
    - Modules get installed into the "node_modules" folder

command:

$ npm install express
$ npm install -g express

[x] Popular Modules
    - Express
      Web development framework
    - Connect
      extensible HTTP server framework
    - Socket.io
      server side component for websockets
    - Pug/Jade 
      Template engine inspired by HAML
    - Mongo / mongose
      Wrappers to interact with MongoDB
    - Coffee-script
      CoffeeScript compiler
    - Redis
      Redis client library

[x] package.json File
    - Goes in the root of your package/application
    - tells npm how your package is structured and 
what to do to install it.

command:
npm init
{
 "name": "mytasklist",
 "version": "1.0.0",
 "description": "simple task manager"
 "main": "server.js",
 "author": "Indra Rahmat",
 "license": "ISC",
 "dependencies": {
   "body-parser": "^1.15.2",
   "express": "^4.14.0",
   "mongojs": "^2.4.0"
 }
}

[x] Using the REPL

[x] NPM - Node Package Manager

[x] How modules work

[x] Package.json file

[x] Basic webserver